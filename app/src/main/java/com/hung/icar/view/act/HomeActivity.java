package com.hung.icar.view.act;

import com.hung.icar.databinding.ActivityHomeBinding;
import com.hung.icar.view.fragment.M000SplashFrg;
import com.hung.icar.viewmodel.CommonVM;

public class HomeActivity extends BaseAct<ActivityHomeBinding, CommonVM> {

    @Override
    public void backToPrevious() {
        onBackPressed();
    }

    @Override
    protected void initViews() {
        showFragment(M000SplashFrg.TAG, null, false);
    }

    @Override
    protected ActivityHomeBinding initViewBinding() {
        return ActivityHomeBinding.inflate(getLayoutInflater());
    }

    @Override
    protected Class<CommonVM> initViewModel() {
        return CommonVM.class;
    }
}